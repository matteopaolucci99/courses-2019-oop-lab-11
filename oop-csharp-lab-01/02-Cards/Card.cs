﻿namespace Cards
{
    class Card
    {

        public Card(string value, string seed)
        {
            this.VALUE = value;
            this.SEED = seed;
        }

        public string VALUE { get; }
        public string SEED { get; }

        public override string ToString()
        {
            // TODO comprendere il meccanismo denominato in C# "string interpolation"
            return $"{this.GetType().Name}(Name={this.VALUE}, Seed={this.SEED})";
        }
    }

    
}
