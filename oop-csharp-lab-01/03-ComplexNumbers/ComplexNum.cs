﻿using System;

namespace ComplexNumbers
{
    class ComplexNum
    {
        public ComplexNum(double re, double im)
        {
            Re = re;
            Im = im;
        }

        public double Re { get; private set; }
        public double Im { get; private set; }

        public double GetModule()
        {
            return Math.Sqrt(Re * Re + Im * Im);
        }

        public ComplexNum GetConjugate()
        {
            return new ComplexNum(Re, Im * -1);
        }

        // Restituisce una rappresentazione idonea per il numero complesso
        public override string ToString()
        {
            return "Real part: " + Re + " imaginary part: " + Im;
        }

        public static ComplexNum operator +(ComplexNum n1, ComplexNum n2)
        {
            return new ComplexNum(n1.Re + n2.Re, n1.Im + n2.Im);
        }

        public static ComplexNum operator -(ComplexNum n1, ComplexNum n2)
        {
            return new ComplexNum(n1.Re - n2.Re, n1.Im - n2.Im);
        }

        public static ComplexNum operator *(ComplexNum n1, ComplexNum n2)
        {
            return new ComplexNum(n1.Re * n2.Re - n1.Im * n2.Im, n1.Re * n2.Im + n1.Im * n2.Re);
        }

        public static ComplexNum operator /(ComplexNum n1, ComplexNum n2)
        {
            return null; //TODO
        }
    }
}
