﻿using System;

namespace Cards
{
    class Deck
    {
        private Card[] cards;

        public Deck()
        {
            cards = new Card[40];
        }

        public void Initialize()
        {
            /*
             * == README ==
             * 
             * I due enumerati (ItalianSeed e ItalianValue) definiti in fondo a questo file rappresentano rispettivamente semi e valori
             * di un tipo mazzo di carte da gioco italiano.
             * 
             * Questo metodo deve inizializzare il mazzo (l'array cards) considerando tutte le combinazioni possibili.
             * 
             * Nota - Dato un generico enumerato MyEnum:
             *   a) è possibile ottenere il numero dei valori presenti con Enum.GetNames(typeof(MyEnum)).Length
             *   b) è possibile ottenere l'elenco dei valori presenti con (MyEnum[])Enum.GetValues(typeof(MyEnum))
             * 
             */

            ItalianValue[] values = (ItalianValue[])Enum.GetValues(typeof(ItalianValue));
            ItalianSeed[] seeds = (ItalianSeed[])Enum.GetValues(typeof(ItalianSeed));


            foreach (int seed in (ItalianSeed[])Enum.GetValues(typeof(ItalianSeed)))
                foreach (int val in (ItalianValue[])Enum.GetValues(typeof(ItalianValue)))
                    cards[seed * 10 + val] = new Card(values[val].ToString(), seeds[seed].ToString());
        }

        public void Print()
        {
            /*
             * == README  ==
             * 
             * Questo metodo stampa tutte le carte presenti nel mazzo, con una propria rappresentazione a scelta.
             */
            foreach (Card card in cards)
                Console.Write("Card: {0} di {1}\n", card.VALUE, card.SEED);
        }

        public Card this[ItalianSeed seed, ItalianValue value] => cards[(int)seed * 10 + (int)value];
    }

    enum ItalianSeed
    {
        DENARI,
        COPPE,
        SPADE,
        BASTONI
    }

    enum ItalianValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        FANTE,
        CAVALLO,
        RE
    }
}
