﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{
    class FrenchDeck
    {
        private Card[] cards;

        public FrenchDeck()
        {
            cards = new Card[54];
        }

        public void Initialize()
        {
            foreach (FrenchSeeds seed in Enum.GetValues(typeof(FrenchSeeds)))
                foreach (FrenchValues val in Enum.GetValues(typeof(FrenchValues))) {
                    if ((int)seed > 3 || (int)val == 13) continue;
                    cards[(int)seed * 13 + (int)val] = new Card(val.ToString(), seed.ToString());
                }

            cards[52] = new Card(FrenchValues.JOLLY.ToString(), FrenchSeeds.JOLLY_ROSSO.ToString());
            cards[53] = new Card(FrenchValues.JOLLY.ToString(), FrenchSeeds.JOLLY_NERO.ToString());
        }

        public void Print()
        {
            foreach (Card card in cards)
                Console.Write("Card: {0} di {1}\n", card.VALUE, card.SEED);
        }

        public Card this[FrenchValues val, FrenchSeeds seed]
        {
            get
            {
                Card c = null;
                foreach (Card card in cards)
                    if (val.ToString() == card.VALUE && seed.ToString() == card.SEED)
                        c = card;
                return c;
            }
        }
    }

    enum FrenchSeeds
    {
        CUORI,
        QUADRI,
        FIORI,
        PICCHE,
        JOLLY_ROSSO,
        JOLLY_NERO
    }

    enum FrenchValues
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        OTTO,
        NOVE,
        DIECI,
        JACK,
        DONNA,
        RE,
        JOLLY        
    }
}
